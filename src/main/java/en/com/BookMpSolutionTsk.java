package en.com;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static en.com.consntant.CommonConstant.INPUT_FILE_NAME;
import static en.com.consntant.CommonConstant.OUTPUT_FILE_NAME;

public class BookMpSolutionTsk {

    public static void main(String[] args) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE_NAME));
             BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUT_FILE_NAME, true))) {

            int numberOfQueries = Integer.parseInt(br.readLine().split(" ")[1]);

            String inputText = br.readLine();

            int l, r, index;
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < numberOfQueries; i++) {
                String[] tokens = br.readLine().split(" ");
                l = Integer.parseInt(tokens[0]) - 1;
                r = Integer.parseInt(tokens[1]) - 1;
                index = Integer.parseInt(tokens[2]) - 1;
                int position = execute(inputText, l, r, index);
                output.append(position).append("\n");
            }
            writer.write(output.toString());
        }
    }

    public static int execute(String text, int l, int r, int index) {

        char targetChar = text.charAt(l + index);
        char otherChar = (targetChar == 'A') ? 'B' : 'A';

        int countTargetChar = 0;
        for (int i = l; i <= l + index; i++) {
            char ch = text.charAt(i);
            if (ch == targetChar) {
                countTargetChar++;
            }
        }

        int position = -1;
        int count = 0;

        for (int i = l; i <= r; i++) {
            if (text.charAt(i) == otherChar) {
                count++;
                if (count == countTargetChar) {
                    position = i - l + 1;
                    return position;
                }
            }
        }
        return position;
    }

}